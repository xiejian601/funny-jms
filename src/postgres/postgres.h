#ifndef _POSTGRES_H
#define _POSTGRES_H

#include "utils.h"
#include "libpq-fe.h"

struct QueryRes{
    _si fields_cnt;
    _si tuples_cnt;
    char **fields;
    char ***tuples;
};

struct Postgres{
    _i (*thread_safe_checker) ();

    Error *(*conn) (const char *, PGconn **restrict) __prm_nonnull __mustuse;
    _i (*conn_checker) (const char *) __prm_nonnull;
    void (*conn_reset) (PGconn *) __prm_nonnull;

    Error *(*query) (PGconn *, const char *, PGresult **) __prm_nonnull __mustuse;
    Error *(*exec) (PGconn *, const char *, PGresult **) __prm_nonnull __mustuse;

    Error *(*query_params) (PGconn *, const char *, _i, const char *const *, PGresult **) __mustuse;
    Error *(*exec_params) (PGconn *, const char *, _i, const char *const *, PGresult **) __mustuse;

    Error *(*prepare) (PGconn *, const char *, const char *, _i, PGresult **) __prm_nonnull __mustuse;
    Error *(*query_prepared) (PGconn *, const char *, _i, const char *const *, PGresult **) __mustuse;
    Error *(*exec_prepared) (PGconn *, const char *, _i, const char *const *, PGresult **) __mustuse;

    void (*query_res_parse) (PGresult *, struct QueryRes *) __prm_nonnull;

    void (*conn_drop) (PGconn *) __prm_nonnull;
    void (*res_drop) (PGresult *, struct QueryRes *);

    Error *(*query_once) (char *, char *, PGresult **, struct QueryRes *) __prm_nonnull __mustuse;
    Error *(*exec_once) (char *, char *) __prm_nonnull __mustuse;

    void (*pool_init) (char *) __prm_nonnull;
    void (*pool_addjob) (void *) __prm_nonnull;
};

struct Postgres postres;

#endif //_POSTGRES_H
